import {
    each,
    map
} from 'lodash';


// Renderers
import Renderer from './renderers/renderer';

// UI Components/Modules
// import Preload from './modules/preload/preload';
import Menu from './modules/menu/menu';
import Header from './modules/header/header';
import Carousel from './modules/carousel/carousel';

// Animations
import Title from './modules/animations/Title';
import Paragraph from './modules/animations/Paragraph';

// User Detect
import Detect from './utils/detect';

// App
import { app } from './utils/store';

import Gallery from './modules/carousel/gallery';

// Init
app.detect = new Detect();
app.detect.init();

app.nav = new Menu();
app.nav.init();

// app.loader = new Preload();
// app.loader.init();

app.renderer = new Renderer();
app.renderer.init();

app.gallery = new Gallery();
app.gallery.init();


app.header = new Header();
app.header.init();

app.carousel = new Carousel();
app.carousel.init();

class App {
    constructor() {
        app.init = true;

        this.createContent();
        this.createAnimations()
    }

    createContent() {
        //Async Load
        this.images = document.querySelectorAll('[data-src]')

        //Animations
        this.animated = {
            titles: document.querySelectorAll('[data-animation="title"]'),
            paragraphs: document.querySelectorAll('[data-animation="paragraph"]'),
            labels: document.querySelectorAll('[data-animation="label"]'),
        }

        // Modals
        this.modal = {
            elements: document.querySelectorAll('[data-modal]'),
            togglers: document.querySelectorAll('[data-modal-target]'),
            closers: document.querySelectorAll('[data-modal-close]')
        }

    }

    //animation
    createAnimations() {
        this.animations = new Array();

        // Titles
        this.animationsTitles = map(this.animated.titles, (element) => {
            return new Title({ element })
        })
        this.animations.push(...this.animationsTitles);

        // Paragraphs
        this.animationsParagraphs = map(this.animated.paragraphs, (element) => {
            return new Paragraph({ element })
        })
        this.animations.push(...this.animationsParagraphs);

        // Labels
        this.animationsLabels = map(this.animated.labels, (element) => {
            return new Label({ element })
        })
        this.animations.push(...this.animationsLabels);
    }
}

new App();