import gsap from 'gsap';
import {
    each
} from 'lodash';
import {
    app
} from '../../utils/store';
import {
    lerp
} from '../../utils/math';


export default class Gallery {
    constructor(element) {
        this.element = document.querySelector('.carousel--wrapper');
        if (this.element) {
            this.initCarousel(this.element);
        }
    }

    init() {}

    initCarousel(entry) {
        const items = entry.querySelectorAll('.carousel--item')
        var images = entry.querySelectorAll('.carousel--item img')

        let carouselWidth = entry.clientWidth
        let itemWidth = items[0].clientWidth
        let wrapperWidth = items.length * itemWidth

        let scrollSpeed = 0
        let oldScrollY = 0
        let scrollY = 0
        let y = 0

        var images = entry.querySelectorAll('.carousel--item img');

        // Set elements
        const setPosition = (scroll) => {
            gsap.set(items, {
                x: (i) => {
                    return i * itemWidth + scroll
                },
                modifiers: {
                    x: (x, target) => {
                        const s = gsap.utils.wrap(-itemWidth, wrapperWidth - itemWidth, parseInt(x))
                        return `${s}px`
                    }
                }
            })
        }

        setPosition(0) // Set initial position
        // setAutoplay()

        const handleMouseWheel = (e) => {
            scrollY -= e.deltaY * 0.9
        }

        let mousedown = false
        let mousedown_timer = undefined
        // Touch
        let touchStart = 0
        let touchX = 0
        let isDragging = false

        const handleTouchStart = (e) => {
            app.device.desktop ? e.preventDefault() : null;
            mousedown = true
            touchStart = e.clientX || e.touches[0].clientX

            mousedown_timer = setTimeout(function () {
                if (mousedown) {
                    isDragging = true
                    entry.classList.add('is-dragging')
                }
            }, 125);
        }

        const handleTouchMove = (e) => {
            if (!isDragging) {
                return
            } else {
                e.preventDefault();
            }
            touchX = e.clientX || e.touches[0].clientX
            scrollY += (touchX - touchStart) * 2.5
            touchStart = touchX
        }

        const handleTouchEnd = () => {
            mousedown = false;
            isDragging = false
            entry.classList.remove('is-dragging');
            if (mousedown_timer) {
                clearTimeout(mousedown_timer);
            }
        }

        // Mouse Wheel
        document.addEventListener('mousewheel', handleMouseWheel)

        // Init Touch Events
        entry.addEventListener('touchstart', handleTouchStart)
        entry.addEventListener('touchmove', handleTouchMove)
        entry.addEventListener('touchend', handleTouchEnd)

        // Init Mouse Events
        entry.addEventListener('mousedown', handleTouchStart)
        entry.addEventListener('mousemove', handleTouchMove)
        entry.addEventListener('mouseleave', handleTouchEnd)
        entry.addEventListener('mouseup', handleTouchEnd)

        entry.addEventListener('selectstart', () => {
            return false
        })


        // Resize
        window.addEventListener('resize', () => {
            carouselWidth = entry.clientWidth
            itemWidth = items[0].clientWidth
            wrapperWidth = items.length * itemWidth
        })

        // Render
        const render = () => {
            requestAnimationFrame(render)

            // if(autoplay){
            // setAutoplay()
            // } else {
            y = lerp(y, scrollY, .1)
            setPosition(y)

            scrollSpeed = y - oldScrollY
            oldScrollY = y

            gsap.to(items, {
                skewX: -scrollSpeed * .05,
                rotate: scrollSpeed * .01,
                scale: 1 - Math.min(100, Math.abs(scrollSpeed)) * 0.003
            })
            // }


        }

        render()

    }

    destroy() {}
}