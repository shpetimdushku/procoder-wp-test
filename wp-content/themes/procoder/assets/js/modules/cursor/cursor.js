import { app } from "../../utils/store"

import gsap from 'gsap'
import { each } from 'lodash'

export default class Cursor {
    create() {
        this.interactive = {
            carousel: document.querySelectorAll('.swiper'),
            arrow: document.querySelectorAll('.btn-swiper'),
        }

        console.log('create-cursor')
        this.html = document.documentElement

        if(!document.getElementById('cursor')){ this.createCursor() } // Create Cursor only if NOT exists
    }

    createCursor(){
        let cursor = document.createElement('div')
        let figure = new Image()

        cursor.id = 'cursor'
        cursor.setAttribute('data-label-default','')
        cursor.setAttribute('data-label','')

        figure.classList = 'cursor--figure'
        figure.alt = 'Cursor Image'
        cursor.append(figure)

        this.html.prepend(cursor)

        this.followMouse(cursor)

         if(this.interactive.carousel){ this.hoverState(this.interactive.carousel) }
         if(this.interactive.arrow){ this.hoverState(this.interactive.arrow)}
    }

    followMouse = (element) => {
        gsap.set(element, {
            xPercent: -50, 
            yPercent: -50
        })

        var position = { 
            x: window.innerWidth / 2, 
            y: window.innerHeight / 2 
        }

        var mouse = { 
            x: position.x,
            y: position.y 
        }

        var speed = 0.1

        var fpms = 60 / 1000

        var xSet = gsap.quickSetter(element, "x", "px")
        var ySet = gsap.quickSetter(element, "y", "px")

        window.addEventListener("mousemove", (event) => {
            mouse.x = event.x
            mouse.y = event.y  
        })

        gsap.ticker.add((time, deltaTime) => {
            var delta = deltaTime * fpms
            var dt = 1.0 - Math.pow(1.0 - speed, delta) 
            
            position.x += (mouse.x - position.x) * dt
            position.y += (mouse.y - position.y) * dt

            xSet(position.x)
            ySet(position.y)
        })
    }

    hoverState = (array) => {
        const cursor = document.getElementById('cursor')
        const figure = cursor.querySelector('.cursor--figure')

        each(array, (entry, key) => {
            var elementLabel = entry.getAttribute('data-label')
            var elementIcon = entry.getAttribute('data-icon')
            var isLink = entry.href != null
            var isButton = entry.localName == 'button'
            var isCarousel = entry.hasAttribute('data-carousel-cursor')
            var isArrow = entry.hasAttribute('data-arrow')
            var isCta = entry.hasAttribute('data-cta')
            // var isArtboard = entry.hasAttribute('data-artboard')

            entry.addEventListener('mouseover', (e) => { 
                // if(elementLabel){ cursor.setAttribute('data-label', elementLabel) }
                // if(elementIcon){ cursor.setAttribute('data-icon', elementIcon) }
                if(isCarousel){ 
                    cursor.classList.add('drag')
                    // cursor.setAttribute('data-label', 'Drag')
                }
                if(isArrow){
                    cursor.classList.remove('drag')
                    cursor.classList.add('arrow')
                    if(entry.hasAttribute('data-prev')){
                        cursor.classList.add('reverse')
                        cursor.classList.remove('drag')
                    }
                }
                if(isLink || isButton){
                    e.stopPropagation()
                    cursor.classList.remove('arrow')
                    cursor.classList.remove('drag')
                }
                // if(isCloser || isModalVideo){ 
                //     cursor.classList.add('closer')
                //     cursor.setAttribute('data-label', '+')
                // }
                // if(isArtboard){ this.animationIn(figure, entry.getAttribute('data-artboard')) }
                cursor.classList.add('active') 
            })

            entry.addEventListener('mouseleave', (e) => { 
                // cursor.setAttribute('data-label', '')
                // cursor.removeAttribute('data-icon')
                if(isArrow){ 
                    cursor.classList.remove('arrow')
                    cursor.classList.remove('reverse')
                }
                if(isCarousel){ cursor.classList.remove('drag') }
                // if(isCloser || isModalVideo){ cursor.classList.remove('closer') }
                // if(isArtboard){ this.animationOut(figure) }
                cursor.classList.remove('active')
                cursor.classList.remove('click')
            })

            entry.addEventListener('mousedown', (e) => { 
                cursor.classList.add('click') 
                cursor.classList.remove('closer') 
            })
            entry.addEventListener('mouseup', (e) => { cursor.classList.remove('click') })
        })
    }

    //ANIMATION IN gsap
    animationIn(image, dataSrc){
        gsap.to(image, {
            duration: .25,
            rotation: -5,
            skewX: -10,
            autoAlpha: 0,
            scale: 0,
            onComplete: () =>{
                dataSrc ? (image.src = dataSrc) : null
                cursor.classList.add('figured')
                this.isHovering = true
            }
        })
        gsap.to(image, {
            duration: .25,
            delay: .5,
            rotation: 0,
            skewX: 0,
            autoAlpha: 1,
            scale: 1,
        })
    }

    //ANIMATION OUT gsap
    animationOut(image){
        gsap.to(image, {
            duration: .25,
            rotation: -5,
            skew: -30,
            autoAlpha: 0,
            scale: 0,
            onComplete: () =>{
                image.removeAttribute('src')
                cursor.classList.remove('figured')
            }
        })
    }

    update = () => {
        
    }

    destroy = () => { 
        
    }
}