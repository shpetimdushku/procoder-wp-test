import { gsap } from 'gsap';
import ScrollTrigger from 'gsap/ScrollTrigger';

gsap.registerPlugin(ScrollTrigger);

export default class Header {
    constructor(){
        this.header = document.querySelector('header');

        this.footer = document.querySelector('footer');

        this.company = document.getElementById('company');
    }

    init(){
        if(Sniff.features.mobile){
            // this.set();
            // this.animation();
        }

        window.onscroll = (event) => this.onScroll();
    }

    set = () => {
        // Set using GSAP
    }

    animation = () => {
        // Animate using GSAP
    }

    onScroll(){  
        if(window.scrollY > this.header.clientHeight){
            this.header.classList.add('active')
        } else {
            this.header.classList.remove('active')
        }
    }
}
