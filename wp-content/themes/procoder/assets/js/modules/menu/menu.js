export default class Menu {
  constructor() {
    this.menu = {
      toggler: document.getElementById('menu-toggler'),
      main: document.getElementById('main-menu'),
      header: document.querySelector('.header'),
    }

    this.lang = {
      toggler: document.querySelector('#select-language .dropdown-toggler'),
      menu: document.querySelector('#select-language .dropdown-menu'),
    }
  }

  init() {
    this.toggle();
    if (this.lang.toggler) {
      this.language();
    }

    this.createAnchorMenu();
  }

  toggle = () => {
    this.menu.toggler.addEventListener('click', () => {
      this.menu.main.classList.toggle('active');
      this.menu.toggler.classList.toggle('active');
      this.menu.header.classList.toggle('active');

      if (this.lang.toggler) {
        this.lang.menu.classList.remove('show');
      }
    });
  }

  language = () => {
    this.lang.toggler.addEventListener('click', () => {
      this.lang.menu.classList.toggle('show');
    });
  }

  createAnchorMenu = () => {
    var topMenu = $(".appartaments-lounges nav"),
      topMenuHeight = topMenu.outerHeight() + 1,
      menuItems = topMenu.find("a"),
      scrollItems = menuItems.map(function () {
        var item = $($(this).attr("href"));
        if (item.length) {
          return item;
        }
      });

    menuItems.click(function (e) {
      var href = $(this).attr("href");

      if (href && href !== "#" && href.startsWith("#")) {
        var offsetTop = $(href).offset().top - topMenuHeight + 1;

        $("html, body").stop().animate({
            scrollTop: offsetTop,
          },
          850
        );
        e.preventDefault();
      }
    });

    $(window).scroll(function () {
      var fromTop = $(this).scrollTop() + topMenuHeight + 100;

      var cur = scrollItems.map(function () {
        if ($(this).offset().top < fromTop) return this;
      });
      cur = cur[cur.length - 1];
      var id = cur && cur.length ? cur[0].id : "";

      menuItems.removeClass("active");
      menuItems.filter("[href='#" + id + "']").addClass("active");

      // Reset the classes of the <nav> element
      topMenu.removeClass().addClass("sala-nav");

      // Get the corresponding lounge section ID
      var activeId = menuItems.filter(".active").attr("href");

      if (activeId && activeId !== "#" && activeId.startsWith("#")) {
        activeId = activeId.substring(1);

        // Generate the dynamic "sala" class based on the lounge section ID
        var loungeTitle = activeId.replace('_', ' ');
        var salaClass = loungeTitle.toLowerCase().replace(/\s/g, '-');

        // Add the dynamic "sala" class to the <nav> element
        topMenu.addClass(salaClass);
      }
    });
  }


}