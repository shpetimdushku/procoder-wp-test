import Sniffer from 'snifferjs';

import slugify from 'slugify';

import {
    app
} from './store';
import UAParser from 'ua-parser-js';

export default class Detect {
    constructor() {
        this.scroll = document.querySelector('.app-scroll');
        this.footer = document.querySelector('footer');

        this.parser = new UAParser();
        this.useragent = this.parser.getUA();
        this.result = this.parser.getResult(this.useragent);

        app.device = {
            desktop: this.result.device.type === undefined,
            tablet: this.result.device.type === 'tablet',
            mobile: this.result.device.type === 'mobile',
        }

        app.browser = {
            name: slugify(this.result.browser.name, {
                lower: true
            }),
            version: slugify(this.result.browser.version, {
                lower: true
            }),
        };

        app.engine = {
            name: slugify(this.result.engine.name, {
                lower: true
            }),
            version: slugify(this.result.engine.version, {
                lower: true
            }),
        };

        app.os = {
            name: slugify(this.result.os.name, {
                lower: true
            }),
            version: slugify(this.result.os.version, {
                lower: true
            }),
        };

        app.cpu = this.result.cpu.architecture;

        // this.browser_check_webp();
        // console.log(app.webp);
        this.appendClasses()

        // window.addEventListener('resize', this.onResize())
    }

    init() {
        var Sniff = Sniffer(navigator.userAgent),
            htmlNode = document.getElementsByTagName('html')[0],
            classNameArr = [htmlNode.className];

        Sniff.browser.name && classNameArr.push(Sniff.browser.name);
        Sniff.browser.engine && classNameArr.push(Sniff.browser.engine);
        Sniff.os.name && classNameArr.push(Sniff.os.name);

        for (var prop in Sniff.features) {
            if (Sniff.features[prop]) classNameArr.push(prop);
        }

        htmlNode.className = classNameArr.join(' ');

        if (typeof (module) != 'undefined' && module.exports) {
            module.exports = Sniff;
        } else {
            global.Sniff = Sniff;
        }
    }

    appendClasses() {
        // OS
        document.documentElement.classList.add(app.os.name);
        document.documentElement.setAttribute('os', app.os.name);
        // Engine
        document.documentElement.classList.add(app.engine.name);
        document.documentElement.setAttribute('engine', app.engine.name);
        // Browser
        document.documentElement.classList.add(app.browser.name);
        document.documentElement.classList.add(app.browser.version);
        // document.documentElement.setAttribute('browser',app.browser.name);
        // Device
        document.documentElement.classList.add(app.device.desktop ? 'desktop' : app.device.tablet ? 'tablet' : app.device.mobile ? 'phone' : '');
        document.documentElement.setAttribute('device', app.device.desktop ? 'desktop' : app.device.tablet ? 'tablet' : app.device.mobile ? 'phone' : '');
        // CPU
        // document.documentElement.classList.add(app.cpu);
        document.documentElement.setAttribute('cpu', app.cpu);
    }

    isPhone() {
        if (!app.PhoneChecked) {
            app.PhoneChecked = true

            app.PhoneCheck = document.documentElement.classList.contains('phone')
        }

        return app.PhoneCheck
    }

    isTablet() {
        if (!app.TabletChecked) {
            app.TabletChecked = true

            app.TabletCheck = document.documentElement.classList.contains('tablet')
        }

        return app.TabletCheck
    }

    isDesktop() {
        if (!app.DesktopChecked) {
            app.DesktopChecked = true

            app.DesktopCheck = document.documentElement.classList.contains('desktop')
        }

        return app.DesktopCheck
    }

    onResize = () => {
        app.device = {
            desktop: this.result.device.type === undefined,
            tablet: this.result.device.type === 'tablet',
            mobile: this.result.device.type === 'mobile',
        }

        this.appendClasses()

        console.log(window.innerWidth);
        console.log(app.device);
    }
}