<?php
/**
 * The Template for displaying all single posts
 *
 * Methods for TimberHelper can be found in the /lib sub-directory
 *
 * @package  WordPress
 * @subpackage  Timber
 * @since    Timber 0.1
 * Template Name: Options
 * Description: Options
 */

$templates = array('pages/options.twig');
$context = Timber::get_context();
$context['post'] = new TimberPost();

$context['posts'] = Timber::get_posts(array(
    'post_type' => 'option_list',
    'numberposts' => -1,
    'orderby' => array('date' => 'ASC' )
));


$slugs = [];
$states = Timber::get_terms('state');

$unique_topics = [];
foreach ($states as $topic) {
    if(!in_array($topic->slug, $slugs))
    {
        $slugs[] = $topic->slug;
        $unique_topics[] = $topic;
    }
}
$context["states"] = $unique_topics;



Timber::render( $templates, $context );
